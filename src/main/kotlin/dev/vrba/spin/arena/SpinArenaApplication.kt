package dev.vrba.spin.arena

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpinArenaApplication

fun main(args: Array<String>) {
	runApplication<SpinArenaApplication>(*args)
}
